face = imread('e:\photos\j001.bmp');
eye = imread('e:\photos\jonnyeye3.png');
% peppers = imread('peppers.png');

imshow(face)
figure, imshow(eye)

% [sub_eye,rect_eye] = imcrop(face);
%[sub_peppers,rect_peppers] = imcrop(peppers);

% figure, imshow(sub_onion)
% figure, imshow(sub_peppers)

c = normxcorr2(eye(:,:,1),face(:,:,1));
figure, surf(c), shading flat

[max_c, imax] = max(abs(c(:)));
[ypeak,xpeak] = ind2sub(size(c),imax(1));
figure(1);
hold on;

rectangle('Position',[(xpeak-50) (ypeak-50) 75 75], 'LineWidth',2, 'EdgeColor','b');


% corr_offset = [(xpeak-size(sub_onion,(2)))
%                (ypeak-size(sub_onion,(1)))];
%            
% rect_offset = [(rect_peppers(1)-rect_onion(1))
%                (rect_peppers(2)-rect_onion(2))];
%            
% offset = corr_offset + rect_offset;
% xoffset = offset(1);
% yoffset = offset(2);
% 
% xbegin = round(xoffset+1);
% xend   = round(xoffset+ size(onion,2));
% ybegin = round(yoffset+1);
% yend   = round(yoffset+size(onion,1));
% 
% % extract region from peppers and compare to onion
% extracted_onion = peppers(ybegin:yend,xbegin:xend,:);
% if isequal(onion,extracted_onion)
%    disp('onion.png was extracted from peppers.png')
% end