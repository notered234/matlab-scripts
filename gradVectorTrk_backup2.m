imgOrig = imread('e:\Photos\aliciaeye1.jpg');
G = fspecial('gaussian', [5 5],2);
imgOrig = imfilter(imgOrig,G,'same');
greyScaleImg = rgb2gray(imgOrig);
[gx,gy] = gradient(double(greyScaleImg));
greyScaleImg = 255-greyScaleImg;
greyScaleImg = im2double(greyScaleImg);
[rmax, cmax] = size(greyScaleImg);
gMag = zeros(rmax,cmax);

for y = 1:rmax;
    for x = 1:cmax;
       gMag(y,x) = sqrt((gx(y,x) * gx(y,x)) + (gy(y,x) * gy(y,x)));
    end
end

%Compute the threshold
stdMagGrad = std2(gMag);
meanGMagrad = mean2(gMag);
gradThreshold = 0.3 * stdMagGrad + meanGMagrad;

%Normalise gradients above threshold to unit length, zero everything else
for y = 1:rmax;
    for x = 1:cmax;
        if gMag(y,x) > gradThreshold;
        gx(y,x) = gx(y,x) / gMag(y,x);
        gy(y,x) = gy(y,x) / gMag(y,x);
        else 
            gx(y,x) = 0.00;
            gy(y,x) = 0.00;
        end
    end
end

sqrdDp = zeros(rmax,cmax);
wSqrdDp = zeros(rmax,cmax);

for y = 1:rmax;
    for x = 1:cmax;
        if gMag(y,x) > gradThreshold;
            for cy = 1:rmax;
                for cx = 1:cmax;
                    dx = x-cx;
                    dy = y-cy;
                    magnitude = sqrt((dx * dx) + (dy * dy));
                    dx = dx / magnitude;
                    dy = dy / magnitude;
                    dotProduct = dx * gx(y,x) + dy * gy(y,x);
                    dotProduct = max(0.0,dotProduct);
                    sqrdDp(cy,cx) = sqrdDp(cy,cx) + dotProduct * dotProduct;
                end
            end
        else 
            continue;
        end
    end
end



for y1 = 1:rmax;
    for x1 = 1:cmax;
        wSqrdDp(y1,x1) = sqrdDp(y1,x1) * I2(y1,x1);
    end
end

[maximum, I] = max(wSqrdDp(:));
threshold = 0.9 * maximum;

for y1 = 1:rmax;
    for x1 = 1:cmax;
        if wSqrdDp(y1,x1) < threshold;
            wSqrdDp(y1,x1) = 0.00;
        end
    end
end

[col, row] = ind2sub(size(wSqrdDp),I);
imgOrig = insertMarker(imgOrig, [row, col]);
imshow(imgOrig);